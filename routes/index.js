var express = require('express');
var router = express.Router();



module.exports = function (app) {
  var api = require('./api')(app);
  router.get('/', function(req, res) {
    res.render('index', {});
  });

  router.use('/api', api);

  return router;
};