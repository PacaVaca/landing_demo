var express = require('express');
var router = express.Router();
var forms = require('forms');
var fields = forms.fields;
var validators = forms.validators;
var ContactEmail = require('./../models/contactEmail');
var http = require('http');

module.exports = function (app) {
  router.post('/contact-us', function (req, res, next) {
    var contactForm = forms.create({
      firstname: fields.string({required: validators.required('Please, provide firstname')}),
      lastname: fields.string({required: validators.required('Please, provide lastname')}),
      email: fields.email({required: validators.required('Please, provide your email')}),
      topic: fields.string({required: validators.required('You should specify the topic')}),
      description: fields.string({required: validators.required('It would be better if I will know the point')}),
      'g-recaptcha-response': fields.string({required: validators.required('Please enter some captcha')})
    });
    contactForm.handle(req, {
      success: function (form) {
       var captchaReq = http.request({
          host: 'www.google.com/recaptcha/api/siteverify',
          method: 'POST',
          path: '/'
        }, function (response) {
          var str = '';
          response.on('data', function (chunk) {
            str += chunk;
          });
          response.on('end', function () {
            console.log(str);
          });
        });
        captchaReq.write(JSON.stringify({
          response: form.data['g-recaptcha-response'],
          secret: app.get('captchaSecret')
        }));
        captchaReq.end();

        var contactEmail = new ContactEmail(form.data);
        contactEmail.save(function (err) {
          if (err) handleError(err);
        });
        
        return res.status(204).json(null);
      },
      error: function (form) {
        return res.status(409).json(null);
      },
      empty: function (form) {
        return res.status(409).json(null);
      }
    })
  });

  return router;
};
