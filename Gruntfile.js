module.exports = function (grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    clean: ['public/javascripts/concat.js'],
    concat: {
      dist: {
        src: 'public/javascripts/*.js',
        dest: 'public/javascripts/concat.js'
      }
    },
    express: {
      options: {
        port: 3000,
        script: './bin/www'
      }
    },
    watch: {
      files: ['public/javascripts/**/*.js', '!public/javascripts/module.js'],
      tasks: ['clean', 'browserify']
    },
    browserify: {
      dist: {
        "transform": [
          ["reactify"]
        ],
        files: {
          'public/javascripts/module.js': ['public/javascripts/**.react.js']
        }
      }
    }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-express-server');
  grunt.loadNpmTasks('grunt-browserify');

  // Default task(s).
  grunt.registerTask('default', ['clean', 'browserify', 'watch']);

};