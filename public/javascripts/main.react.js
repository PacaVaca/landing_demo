var React = require('react/addons');

$(document).ready(function () {
  var App = require('./components/App.react');
  React.render(React.createElement(App, null), document.getElementById('react-app'));
});
