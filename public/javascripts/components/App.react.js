var React = require('react/addons');
var TechnologyModal = require('./reactApp/TechnologyModal.react');
var Navbar = require('./reactApp/Navbar.react');
var Introduction = require('./reactApp/Introduction.react');
var ProjectList = require('./reactApp/ProjectList.react');
var ContactUsForm = require('./reactApp/forms/ContactUsForm.react');

module.exports = App = React.createClass({
  displayName: 'App',

  currentUser: {
    id: 1,
    username: 'Paul Seleznev',
    authorized: false
  },
  subscribe: function () {
    console.log('subscribed');
  },
  authorize: function (type) {
    var currentUser = this.currentUser;
    currentUser.authorized = true;
    this.setState({
      currentUser: currentUser
    });
  },
  setEmail: function (emailString) {
    this.setState({email: emailString});
  },

  getInitialState: function () {
    var projects = [
      {
        title: 'Project #2',
        img: 'images/main-image.jpg',
        startDate: '2010-01-01',
        endDate: '2010-01-01',
        description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu finibus ligula. ' +
          'Maecenas interdum pulvinar tincidunt. Donec non feugiat enim, non vehicula risus. ' +
          'Nullam varius gravida nisi, scelerisque tincidunt mi pretium non. Integer ut justo ex. ' +
          'Fusce eget blandit libero. Pellentesque habitant morbi tristique senectus et netus et ' +
          'malesuada fames ac turpis egestas. Pellentesque sed nisl quis libero maximus cursus.' +
          ' Nullam consectetur nisi orci, in hendrerit quam finibus quis. ' +
          'Cras enim ante, sollicitudin in dictum et, ullamcorper nec augue. Duis sodales quam vel ante varius rhoncus.' +
          ' Phasellus nec tortor eu eros iaculis venenatis. Praesent quis metus sit amet eros molestie placerat. ' +
          'Vivamus molestie diam lacus, vel feugiat augue venenatis eget. Ut rhoncus convallis orci at interdum.',
        technologies: [
          {title: 'Scala', experience: 'Experience: 5 years'},
          {title: 'AngularJS', experience: 'Experience: 5 years'},
          {title: 'PHP', experience: 'Experience: 5 years'},
          {title: 'CSS', experience: 'Experience: 5 years'},
          {title: 'HTML', experience: 'Experience: 5 years'},
          {title: 'Solr', experience: 'Experience: 5 years'},
          {title: 'Memcached', experience: 'Experience: 5 years'},
          {title: 'jQuery', experience: 'Experience: 5 years'}
        ]
      },
      {
        title: 'Project #2',
        img: 'images/main-image.jpg',
        startDate: '2010-01-01',
        endDate: '2010-01-01',
        description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu finibus ligula. ' +
        'Maecenas interdum pulvinar tincidunt. Donec non feugiat enim, non vehicula risus. ' +
        'Nullam varius gravida nisi, scelerisque tincidunt mi pretium non. Integer ut justo ex. ' +
        'Fusce eget blandit libero. Pellentesque habitant morbi tristique senectus et netus et ' +
        'malesuada fames ac turpis egestas. Pellentesque sed nisl quis libero maximus cursus.' +
        ' Nullam consectetur nisi orci, in hendrerit quam finibus quis. ' +
        'Cras enim ante, sollicitudin in dictum et, ullamcorper nec augue. Duis sodales quam vel ante varius rhoncus.' +
        ' Phasellus nec tortor eu eros iaculis venenatis. Praesent quis metus sit amet eros molestie placerat. ' +
        'Vivamus molestie diam lacus, vel feugiat augue venenatis eget. Ut rhoncus convallis orci at interdum.',
        technologies: [
          {title: 'Scala', experience: 'Experience: 5 years'},
          {title: 'React.js', experience: 'Experience: 5 years'},
          {title: 'MongoDB', experience: 'Experience: 5 years'},
          {title: 'Symfony2', experience: 'Experience: 5 years'},
          {title: 'Solr', experience: 'Experience: 5 years'},
          {title: 'Memcached', experience: 'Experience: 5 years'},
          {title: 'jQuery', experience: 'Experience: 5 years'}
        ]
      },
      {
        title: 'Project #3',
        img: 'images/main-image.jpg',
        startDate: '2010-01-01',
        endDate: '2010-01-01',
        description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu finibus ligula. ' +
        'Maecenas interdum pulvinar tincidunt. Donec non feugiat enim, non vehicula risus. ' +
        'Nullam varius gravida nisi, scelerisque tincidunt mi pretium non. Integer ut justo ex. ' +
        'Fusce eget blandit libero. Pellentesque habitant morbi tristique senectus et netus et ' +
        'malesuada fames ac turpis egestas. Pellentesque sed nisl quis libero maximus cursus.' +
        ' Nullam consectetur nisi orci, in hendrerit quam finibus quis. ' +
        'Cras enim ante, sollicitudin in dictum et, ullamcorper nec augue. Duis sodales quam vel ante varius rhoncus.' +
        ' Phasellus nec tortor eu eros iaculis venenatis. Praesent quis metus sit amet eros molestie placerat. ' +
        'Vivamus molestie diam lacus, vel feugiat augue venenatis eget. Ut rhoncus convallis orci at interdum.',
        technologies: [
          {title: 'Scala', experience: 'Experience: 5 years'},
          {title: 'React.js', experience: 'Experience: 5 years'},
          {title: 'MongoDB', experience: 'Experience: 5 years'},
          {title: 'Symfony2', experience: 'Experience: 5 years'},
          {title: 'Memcached', experience: 'Experience: 5 years'},
          {title: 'jQuery', experience: 'Experience: 5 years'}
        ]
      },
      {
        title: 'Project #4',
        img: 'images/main-image.jpg',
        startDate: '2010-01-01',
        endDate: '2010-01-01',
        description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu finibus ligula. ' +
        'Maecenas interdum pulvinar tincidunt. Donec non feugiat enim, non vehicula risus. ' +
        'Nullam varius gravida nisi, scelerisque tincidunt mi pretium non. Integer ut justo ex. ' +
        'Fusce eget blandit libero. Pellentesque habitant morbi tristique senectus et netus et ' +
        'malesuada fames ac turpis egestas. Pellentesque sed nisl quis libero maximus cursus.' +
        ' Nullam consectetur nisi orci, in hendrerit quam finibus quis. ' +
        'Cras enim ante, sollicitudin in dictum et, ullamcorper nec augue. Duis sodales quam vel ante varius rhoncus.' +
        ' Phasellus nec tortor eu eros iaculis venenatis. Praesent quis metus sit amet eros molestie placerat. ' +
        'Vivamus molestie diam lacus, vel feugiat augue venenatis eget. Ut rhoncus convallis orci at interdum.',
        technologies: [
          {title: 'Scala', experience: 'Experience: 5 years'},
          {title: 'AngularJS', experience: 'Experience: 5 years'},
          {title: 'PHP', experience: 'Experience: 5 years'},
          {title: 'CSS', experience: 'Experience: 5 years'},
          {title: 'HTML', experience: 'Experience: 5 years'},
          {title: 'React.js', experience: 'Experience: 5 years'},
          {title: 'Memcached', experience: 'Experience: 5 years'}
        ]
      },
      {
        title: 'Project #3',
        img: 'images/main-image.jpg',
        startDate: '2010-01-01',
        endDate: '2010-01-01',
        description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu finibus ligula. ' +
        'Maecenas interdum pulvinar tincidunt. Donec non feugiat enim, non vehicula risus. ' +
        'Nullam varius gravida nisi, scelerisque tincidunt mi pretium non. Integer ut justo ex. ' +
        'Fusce eget blandit libero. Pellentesque habitant morbi tristique senectus et netus et ' +
        'malesuada fames ac turpis egestas. Pellentesque sed nisl quis libero maximus cursus.' +
        ' Nullam consectetur nisi orci, in hendrerit quam finibus quis. ' +
        'Cras enim ante, sollicitudin in dictum et, ullamcorper nec augue. Duis sodales quam vel ante varius rhoncus.' +
        ' Phasellus nec tortor eu eros iaculis venenatis. Praesent quis metus sit amet eros molestie placerat. ' +
        'Vivamus molestie diam lacus, vel feugiat augue venenatis eget. Ut rhoncus convallis orci at interdum.',
        technologies: [
          {title: 'Scala', experience: 'Experience: 5 years'},
          {title: 'React.js', experience: 'Experience: 5 years'},
          {title: 'MongoDB', experience: 'Experience: 5 years'},
          {title: 'Symfony2', experience: 'Experience: 5 years'},
          {title: 'Memcached', experience: 'Experience: 5 years'},
          {title: 'jQuery', experience: 'Experience: 5 years'}
        ]
      },
      {
        title: 'Project #3',
        img: 'images/main-image.jpg',
        startDate: '2010-01-01',
        endDate: '2010-01-01',
        description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu finibus ligula. ' +
        'Maecenas interdum pulvinar tincidunt. Donec non feugiat enim, non vehicula risus. ' +
        'Nullam varius gravida nisi, scelerisque tincidunt mi pretium non. Integer ut justo ex. ' +
        'Fusce eget blandit libero. Pellentesque habitant morbi tristique senectus et netus et ' +
        'malesuada fames ac turpis egestas. Pellentesque sed nisl quis libero maximus cursus.' +
        ' Nullam consectetur nisi orci, in hendrerit quam finibus quis. ' +
        'Cras enim ante, sollicitudin in dictum et, ullamcorper nec augue. Duis sodales quam vel ante varius rhoncus.' +
        ' Phasellus nec tortor eu eros iaculis venenatis. Praesent quis metus sit amet eros molestie placerat. ' +
        'Vivamus molestie diam lacus, vel feugiat augue venenatis eget. Ut rhoncus convallis orci at interdum.',
        technologies: [
          {title: 'Scala', experience: 'Experience: 5 years'},
          {title: 'React.js', experience: 'Experience: 5 years'},
          {title: 'MongoDB', experience: 'Experience: 5 years'},
          {title: 'Symfony2', experience: 'Experience: 5 years'},
          {title: 'Memcached', experience: 'Experience: 5 years'},
          {title: 'jQuery', experience: 'Experience: 5 years'}
        ]
      }
    ];

    var technologies = [
      {title: 'Scala', experience: 'Experience: 5 years'},
      {title: 'React.js', experience: 'Experience: 5 years'},
      {title: 'JavaScript', experience: 'Experience: 5 years'},
      {title: 'MongoDB', experience: 'Experience: 5 years'},
      {title: 'Memcached', experience: 'Experience: 5 years'},
      {title: 'jQuery', experience: 'Experience: 5 years'},
      {title: 'PHP', experience: 'Experience: 5 years'},
      {title: 'HTML', experience: 'Experience: 5 years'},
      {title: 'CSS', experience: 'Experience: 5 years'},
      {title: 'HTML5', experience: 'Experience: 5 years'},
      {title: 'CSS3', experience: 'Experience: 5 years'},
      {title: 'Apache', experience: 'Experience: 5 years'},
      {title: 'Apache Solr', experience: 'Experience: 5 years'}
    ];

    return {
      subscribed: false,
      email: '',
      currentUser: this.currentUser,
      technologies: technologies,
      projects: projects
    };
  },

  technologiesModalToggle: function (technologies) {
    this.setState({
      technologies: technologies
    });
    $('#technologies-modal').modal('toggle');
  },

  render: function () {
    var _ = React.DOM;
    return (
      _.div({id: 'wrapper'},
        React.createElement(TechnologyModal, {technologies: this.state.technologies}),
        _.div({id: 'header', className: 'container-fluid'},
          _.div({className: 'row'},
            _.div({className: 'col-xs-12'},
              React.createElement(Navbar, {technologiesModalToggle: this.technologiesModalToggle})
            )
          )
        ),
        React.createElement('div', {id: 'body', className: 'container-fluid'},
          React.createElement('div', {className: 'row bottom-space-60'},
            React.createElement(Introduction, {technologiesModalToggle: this.technologiesModalToggle, skipTop: 65})

            //React.createElement('div', {className: 'bottom-space-60'},
            //  React.createElement(HeroUnit, {
            //    subscribed: this.state.subscribed,
            //    subscribe: this.subscribe,
            //    setEmail: this.setEmail,
            //    email: this.state.email,
            //    skipHeight: 64
            //  })
            //)
          ),
          _.div({className: 'row bottom-space-60', id: 'portfolio'},
            _.h2({className: 'text-center'}, 'Portfolio...'),
            React.createElement(ProjectList, {
              projects: this.state.projects,
              technologiesModalToggle: this.technologiesModalToggle
            })
          ),
          _.div({className: 'container-fluid'},
            _.div({className: 'row bottom-space-60'},
              _.h2({className: 'text-center'}, 'Contact Form...'),
              React.createElement(ContactUsForm, null)
            )
          )
        )
      )
    );
  }
});
