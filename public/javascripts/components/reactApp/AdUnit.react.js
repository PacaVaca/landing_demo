var React = require('react/addons');

module.exports = AdUnit = React.createClass({
  displayName: 'AdUnit',

  render: function () {
    return (
      React.createElement('article', {className: 'ad-unit col-xs-12 col-md-6'},
        React.createElement('div', {className: 'page-header'},
          React.createElement('h3', {className: 'title'}, 'Title')
        ),
        React.createElement('div', {className: 'body'},
          'Lorem ipsum dolor sit amet, ' +
          'consectetur adipiscing elit. ' +
          'Vivamus condimentum nisi vitae efficitur semper.Praesent ante ante, laoreet vel odio a, congue dictum enim. ' +
          'Morbi interdum massa vitae faucibus blandit. Suspendisse augue leo, semper nec velit sollicitudin, ' +
          'fermentum suscipit enim. Maecenas semper justo id aliquam rutrum. Nam eget lectus orci. ' +
          'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. ' +
          'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; ' +
          'Pellentesque id egestas dolor, id placerat mauris. Integer cursus bibendum dignissim. ' +
          'Quisque urna tellus, fringilla a ligula eu, pellentesque fringilla nisl. ' +
          'Sed sollicitudin lorem ut metus ultricies porta.'
        )
      )
    );
  }
});