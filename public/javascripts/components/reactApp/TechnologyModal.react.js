var React = require('react/addons');
var TechnologyList = require('./TechnologyList.react');

module.exports = TechnologyModal = React.createClass({
  displayName: 'TechnologyModal',
  getTechnologies: function () {
    var _ = React.DOM;
    var currentTechnologies = [];
    var createTechnologyComponentList = function () {
      console.log(this.state.technologies);
      return null;
    }
  },

  technologies: [],

  getInitialState: function () {
    this.technologies = this.props.technologies;
    return {
      technologies: this.props.technologies
    }
  },

  render: function () {
    var _ = React.DOM;
    var currentTechnologies = [];
    if (!!this.props.technologies && !!this.props.technologies.length) {
      currentTechnologies = this.props.technologies;
    } else {
      currentTechnologies = this.technologies;
    }

    return (
      _.div({className: 'modal fade', id: 'technologies-modal', role: 'dialog'},
        _.div({className: 'modal-dialog modal-lg'},
          _.div({className: 'modal-content'},
            _.div({className: 'modal-header'},
              _.button({type: 'button', className: 'close', 'data-dismiss': 'modal', 'area-label': 'Close'},
                _.span({'aria-hidden': true}, 'x')
              ),
              _.h4({className: 'modal-title'}, 'Technologies...')
            ),
            _.div({className: 'modal-body text-center'},
              React.createElement(TechnologyList, {technologies: currentTechnologies})
            )
          )
        )
      )
    )
  }
});