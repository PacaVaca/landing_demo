var React = require('react/addons');

module.exports = AdList = React.createClass({
  displayName: 'AdList',
  render: function () {
    return (
      React.createElement('div', {className: 'container', id: 'ad-list'},
        React.createElement('div', {className: 'row'},
          React.createElement(AdUnit),
          React.createElement(AdUnit),
          React.createElement(AdUnit),
          React.createElement(AdUnit)
        )
      )
    );
  }
});