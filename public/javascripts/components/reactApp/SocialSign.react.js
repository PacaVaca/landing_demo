var React = require('react/addons');

module.exports = SocialSign = React.createClass({
  displayName: 'SocialSign',
  
  authorizeByFacebook: function (event) {
    this.state.authorize('facebook');
  },
  authorizeByVK: function (event) {
    this.state.authorize('vk');
  },
  authorizeByGoogle: function (event) {
    this.state.authorize('google');
  },

  getInitialState: function () {
    return {
      authorize: this.props.authorize
    }
  },
  render: function () {
    return (
      React.createElement('div', {id: 'social-sign'},
        React.createElement('div', {className: 'button-list'},
          React.createElement('a', {onClick: this.authorizeByFacebook, ref: 'facebook', className: 'col-xs-12 col-md-8 col-md-offset-2 btn btn-default'},
            React.createElement('i', {className: 'fa fa-3x fa-facebook'}),
            React.createElement('span', {className: 'title'}, 'Facebook')
          ),
          React.createElement('a', {onClick: this.authorizeByVK, ref: 'vk', className: 'col-xs-12 col-md-8 col-md-offset-2 btn btn-default'},
            React.createElement('i', {className: 'fa fa-3x fa-vk'}),
            React.createElement('span', {className: 'title'}, 'VK')
          ),
          React.createElement('a', {onClick: this.authorizeByGoogle, ref: 'google', className: 'col-xs-12 col-md-8 col-md-offset-2 btn btn-default'},
            React.createElement('i', {className: 'fa fa-3x fa-google-plus'}),
            React.createElement('span', {className: 'title'}, 'Google+')
          )
        )
      )
    );
  }
});