var React = require('react/addons');

module.exports = CommentForm = React.createClass({
  displayName: 'CommentForm',
  getInitialState: function () {
    return {
      user: this.props.user,
      authorize: this.props.authorize
    };
  },
  render: function () {
    var displayForm = this.state.user.authorized ? 'block' : 'none';
    var displaySignIn = this.state.user.authorized ? 'none': 'block';
    return (
      React.createElement('div', {className: 'container'},
        React.createElement('div', {className: 'row'},
          React.createElement('div', {style: {display: displaySignIn}},
            React.createElement(SocialSign, {style: {display: displaySignIn}, authorized: !!this.state.user.authorized, authorize: this.state.authorize})
          ),
          React.createElement('div', {style: {display: displayForm}, id: 'comment-form', className: 'col-xs-12'},
            React.createElement('form', {className: 'form-horizontal'},
              React.createElement('div', {className: 'form-group'},
                React.createElement('div', {className: 'row'},
                  React.createElement('div', {className: 'col-xs-12 col-md-8 col-md-offset-2'},
                    React.createElement('textarea', {className: 'form-control', placeholder: 'Enter your comment', rows: "5", id: 'comment-textarea'}),
                    React.createElement('div', {className: 'help-block'},
                      React.createElement('span', null, 'If it\'s not your account. '),
                      React.createElement('span', null,
                        React.createElement('a', {href: '#'}, 'Please logout if you kind?')
                      )
                    )
                  )
                )
              ),
              React.createElement('div', {className: 'form-group'},
                React.createElement('button', {className: 'btn btn-primary btn-lg col-xs-12 col-md-8 col-md-offset-2'}, 'Comment')
              )
            )
          )
        )
      )
    )
  }
});