var React = require('react/addons');

module.exports = Loader = React.createClass({
  displayName: 'Loader',
  getInitialState: function () {
    return {
      showLoader: this.props.showLoader
    };
  },
  render: function () {
    var _ = React.DOM;
    var showLoader = this.props.showLoader ? 'block' : 'none';
    return (
      _.div({className: 'loader-container', style: {display: showLoader}},
        _.div({className: 'background'}),
        _.div({className: 'spinner'},
          _.div({className: 'dot1'}),
          _.div({className: 'dot1'})
        )
      )
    );
  }
});