var React = require('react/addons');
var Formsy = require('formsy-react');
var ContactUsFirstnameInput = require('./inputs/ContactUsFirstnameInput.react');
var ContactUsLastnameInput = require('./inputs/ContactUsLastnameInput.react');
var ContactUsEmailInput = require('./inputs/ContactUsEmailInput.react');
var ContactUsTopicInput = require('./inputs/ContactUsTopicInput.react');
var ContactUsDescriptionInput = require('./inputs/ContactUsDescriptionInput.react');
var CaptchaInput = require('./inputs/CaptchaInput.react');
var Loader = require('./Loader.react.js');


module.exports = ContactUsForm = React.createClass({
  displayName: 'ContactUsForm',
  onSubmitSuccess: function () {

  },
  onSubmitError: function () {
    console.log('error');
  },
  enableButton: function () {
    this.setState({
      canSubmit: true
    });
  },
  disableButton: function () {
    this.setState({
      canSubmit: false
    });
  },
  getInitialState: function () {
    return {
      canSubmit: false,
      loaderActive: false
    };
  },
  onSubmitted: function () {
    this.setState({
      loaderActive: false
    });
  },
  send: function (form) {
    form['g-recaptcha-response'] = $('#g-recaptcha-response').val();

    $.ajax({
      url: '/api/contact-us',
      method: 'POST',
      data: form,
      dataType: 'json'
    })
      .success(function (response) {
        console.log('success');
      })
      .error(function (response) {
        console.log('error');
      });

    this.setState({
      loaderActive: true
    });
  },
  render: function () {
    var _ = React.DOM;
    var showLoader = !!this.state.loaderActive;
    return (
      _.div({id: 'contact-form'},
        _.div({className: 'background-image'}),
        _.div({className: 'col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4'},
          _.div({className: 'form-container row'},
            React.createElement(Loader, {showLoader: showLoader}),
            React.createElement(Formsy.Form, {
                name: 'contactForm',
                className: '',
                onError: this.onSubmitError,
                onSuccess: this.onSubmitSuccess,
                onSubmit: this.send,
                onSubmitted: this.onSubmitted,
                onValid: this.enableButton,
                onInvalid: this.disableButton
              },
              React.createElement(ContactUsFirstnameInput, {
                name: 'firstname',
                required: true,
                validations: 'isAlpha,isLength:2:20',
                validationError: 'Your first name please'
              }),
              React.createElement(ContactUsLastnameInput, {
                name: 'lastname',
                required: true,
                validations: 'isAlpha,isLength:2:20',
                validationError: 'Your last name please'
              }),
              React.createElement(ContactUsEmailInput, {
                name: 'email',
                'required': true,
                validations: 'isEmail',
                validationError: 'Not empty, valid email'
              }),
              React.createElement(ContactUsTopicInput, {
                name: 'topic',
                required: true,
                validations: 'isValue',
                validationError: 'Choose a topic to speak about'
              }),
              React.createElement(ContactUsDescriptionInput, {
                name: 'description',
                required: true,
                validations: 'isValue,isLength:128:1024',
                validationError: 'A couple of works about the issue > 128 and < 1024 words'
              }),
              React.createElement(CaptchaInput, {
                name: 'g-recaptcha-response',
                required: true,
                validations: 'isValue'
              }),

              _.button({
                className: 'btn col-xs-8 col-xs-offset-2 btn-lg btn-primary',
                disabled: !this.state.canSubmit
              }, 'Send')
            )
          )
        )
      )
    );
  }
});