var React = require('react/addons');
module.exports = CaptchaInput = React.createClass({
  callback: function () {
    console.log('captcha callback');
  },
  verifyCallback: function () {
    console.log('verification captcha');
  },
  displayName: 'CaptchaInput',
  render: function () {
    var _ = React.DOM;
    return (
      _.div({className: 'form-group col-lg-12 text-center'},
        _.div({
          className: 'g-recaptcha',
          'data-sitekey': '6Lc44AMTAAAAANI73AsaceYqO8XvEkCS4mHJrdhC',
          'data-theme': 'light',
          'data-callback': this.verifyCallback
        })
      )
    )
  }
});