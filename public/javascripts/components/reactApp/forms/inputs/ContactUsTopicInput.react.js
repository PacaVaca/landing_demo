var React = require('react/addons');

module.exports = ContactUsTopicInput = React.createClass({
  displayName: 'ContactUsTopicInput',

  mixins: [Formsy.Mixin],

  changeValue: function (event) {
    this.setValue(event.currentTarget.value);
  },
  render: function () {
    var _ = React.DOM;
    var error = this.getErrorMessage();

    return (
      _.div({className: 'form-group col-lg-12'},
        _.label({for: 'contact-topic'}, 'Topic:'),
        _.select({
            ref: 'topic',
            className: 'form-control',
            id: 'contact-topic',
            required: this.isRequired(),
            value: this.getValue(),
            onChange: this.changeValue
          },
          _.option(null, 'Select topic'),
          _.option({value: 'new'}, 'New'),
          _.option({value: 'support'}, 'Support')
        ),
        _.div({className: 'well well-sm show  '},
          _.span(null, error)
        )
      )
    )
  }
});