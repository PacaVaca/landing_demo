var React = require('react/addons');

module.exports = ContactUsLastnameInput = React.createClass({
  displayName: 'ContactUsLastnameInput',

  mixins: [Formsy.Mixin],

  changeValue: function (event) {
    this.setValue(event.currentTarget.value);
  },
  render: function () {
    var _ = React.DOM;
    var error = this.getErrorMessage();
    var displayError = this.isValid() ? 'none' : 'block';

    return (
      _.div({className: 'form-group col-xs-12 col-md-6'},
        _.label({for: 'contact-lastname'}, 'Last name:'),
        _.input({
          required: 'required',
          type: 'text',
          ref: 'lastname',
          className: 'form-control',
          id: 'contact-lastname',
          value: this.getValue(),
          onChange: this.changeValue,
          placeholder: 'Enter your lastname'
        }),
        _.div({className: 'well well-sm show  '},
          _.span(null, error)
        )
      )
    )
  }
});