var React = require('react/addons');

module.exports = ContactUsEmailInput = React.createClass({
  displayName: 'ContactUsEmailInput',

  mixins: [Formsy.Mixin],

  changeValue: function (event) {
    this.setValue(event.currentTarget.value);
  },
  render: function () {
    var _ = React.DOM;
    var error = this.getErrorMessage();

    return (
      _.div({className: 'form-group col-lg-12'},
        _.label({for: 'contact-email'}, 'Email:'),
        _.input({
          required: this.isRequired(),
          type: 'email',
          ref: 'email',
          className: 'form-control',
          value: this.getValue(),
          onChange: this.changeValue,
          id: 'contact-email',
          placeholder: 'Enter your email'
        }),
        _.div({className: 'well well-sm show  '},
          _.span(null, error)
        )
      )
    );
  }
});