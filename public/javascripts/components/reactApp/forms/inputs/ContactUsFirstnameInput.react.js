var React = require('react/addons');

module.exports = ContactUsFirstnameInput = React.createClass({
  displayName: 'ContactUsFirstnameInput',

  mixins: [Formsy.Mixin],

  changeValue: function (event) {
    this.setValue(event.currentTarget.value);
  },
  render: function () {
    var _ = React.DOM;
    var error = this.getErrorMessage();

    return (
      _.div({className: 'form-group col-xs-12 col-md-6'},
        _.label({for: 'contact-firstname'}, 'First name:'),
        _.input({
          type: 'text',
          ref: 'firstname',
          className: 'form-control',
          required: this.isRequired(),
          id: 'contact-firstname',
          value: this.getValue(),
          onChange: this.changeValue,
          placeholder: 'Enter your firstname'
        }),
        _.div({className: 'well well-sm show  '},
          _.span(null, error)
        )
      )
    )
  }
});