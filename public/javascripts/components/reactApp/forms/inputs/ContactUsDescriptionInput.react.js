var React = require('react/addons');

module.exports = ContactUsDescriptionInput = React.createClass({
  displayName: 'ContactUsDescriptionInput',

  mixins: [Formsy.Mixin],

  changeValue: function (event) {
    this.setValue(event.currentTarget.value);
  },
  render: function () {
    var _ = React.DOM;
    var error = this.getErrorMessage();

    return (
      _.div({className: 'form-group col-lg-12'},
        _.label({for: 'contact-description'}, 'Description:'),
        _.textarea({
          onChange: this.changeValue,
          ref: 'description',
          required: this.isRequired(),
          className: 'form-control',
          placeholder: 'Tell me more information about the question',
          rows: 5
        }, this.getValue()),
        _.div({className: 'well well-sm show  '},
          _.span(null, error)
        )
      )
    )
  }
});