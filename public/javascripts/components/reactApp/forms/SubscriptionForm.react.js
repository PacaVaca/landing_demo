var React = require('react/addons');

module.exports = SubscriptionForm = React.createClass({
  displayName: 'SubscriptionForm',

  id: '#subscription-form',

  submitForm: function (event) {
    event.preventDefault();
    console.log(this.refs.email.getDOMNode().value);
    this.props.subscribe();
  },
  emailEnterHandler: function (event) {
    var emailValue = React.findDOMNode(this.refs.email).value.trim();
    this.setState({email: emailValue});
    this.props.setEmail(emailValue);
  },
  getInitialState: function () {
    var style = {};
    if (this.props.dynamic) {
      var height = 0;
      if (this.props.dynamic && this.props.parentHeight) {
        if (this.props.skipHeight) {
          height = this.props.parentHeight / 3 - this.props.skipHeight;
        } else {
          height = this.props.parentHeight / 3;
        }
        style.marginTop = this.props.parentHeight / 2 - height / 2;
      }
      style.height = height;
    } else {
      style.height = '100%';
    }

    return {
      showTitle: !!this.props.showTitle,
      style: style
    };
  },
  render: function () {
    var formStyle = {};
    var className = 'text-center';
    var form = $(this.id);


    if (this.props.className) {
      className = this.props.className + ' text-center'
    }

    return (
      React.createElement('form', {
          id: this.id,
          className: className,
          onSubmit: this.submitForm,
          style: this.state.style
        },
        React.createElement('div', {className: 'col-xs-12'},
          React.createElement('h2', {className: this.state.showTitle ? '' : 'hidden'}, 'Form title')
        ),
        React.createElement('div', {className: 'col-xs-12'},
          React.createElement('div', {className: 'form-group'},
            React.createElement('input', {
              className: 'form-control input-lg text-center',
              type: 'email',
              name: 'email',
              placeholder: 'Enter your email',
              autoComplete: 'off',
              ref: 'email',
              value: this.props.email,
              onChange: this.emailEnterHandler
            })
          )
        ),
        React.createElement('div', {className: 'col-xs-12'},
          React.createElement('div', {className: 'form-group'},
            React.createElement('a', {
              href: '#',
              className: this.props.btnClass + ' btn btn-default btn-lg',
              type: 'submit'
            }, 'Subscribe')
          )
        )
      )
    );
  }
});