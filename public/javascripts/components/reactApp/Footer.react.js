var React = require('react/addons');

module.exports = Footer = React.createClass({
  displayName: 'Footer',

  render: function () {
    return (
      React.createElement('footer', {className: 'footer text-center', id: 'footer'},
        React.createElement('div', {className: 'container'},
          React.createElement('div', {className: 'row'},
            React.createElement('div', {className: 'col-xs-12 col-md-4 social-container'},
              React.createElement('div', {className: 'row'},
                React.createElement('div', {className: 'col-xs-4'},
                  React.createElement('a', {className: 'facebook-link', href: '#'},
                    React.createElement('i', {className: 'fa fa-3x fa-facebook'})
                  )
                ),
                React.createElement('div', {className: 'col-xs-4'},
                  React.createElement('a', {className: 'vk-link', href: '#'},
                    React.createElement('i', {className: 'fa fa-3x fa-vk'})
                  )
                ),
                React.createElement('div', {className: 'col-xs-4'},
                  React.createElement('a', {className: 'instagram-link', href: '#'},
                    React.createElement('i', {className: 'fa fa-3x fa-instagram'})
                  )
                )
              )
            ),
            React.createElement('div', {className: 'col-xs-12 col-md-8'},
              React.createElement('div', {className: 'row'},
                React.createElement('div', {className: 'col-xs-12'},
                  React.createElement(SubscriptionForm, {
                    subscribe: this.props.subscribe,
                    setEmail: this.props.setEmail,
                    email: this.props.email,
                    className: 'form-inline',
                    showTitle: false
                  })
                )
              )
            )
          )
        )
      )
    );
  }
});