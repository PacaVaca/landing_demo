var React = require('react/addons');

module.exports = Quote = React.createClass({
  displayName: 'Quote',

  id: 'random-quote',
  marginTop: 20,
  show: false,
  xhrInProgress: false,
  update: function () {
    if (this.xhrInProgress) {
      return;
    }
    this.xhrInProgress = true;
    $(this.refs.quote.getDOMNode()).toggleClass('fadeout');
    this.setState({
      show: false
    });
    return $.ajax({
      url: 'https://andruxnet-random-famous-quotes.p.mashape.com/cat=budda',
      method: 'POST',
      data: null,
      headers: {
        'X-Mashape-Key': 'Og9iNzDHywmshv7gEDRjZXcgsvlLp1XHfsdjsn7VoObZyAtZmS',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
      },
      context: this
    })
      .success(function (quote) {
        var data = JSON.parse(quote);
        $(this.refs.quote.getDOMNode()).toggleClass('fadeout');
        this.setState({
          quote: data.quote,
          author: data.author,
          show: true
        })
      })
      .always(function () {
        this.xhrInProgress = false
      });
  },

  getInitialState: function () {
    return {
      author: this.props.author || '',
      quote: this.props.quote || '',
      show: false
    }
  },

  componentDidMount: function () {
    this.interval = setInterval(this.update, 7000);
  },
  componentWillUpmount: function () {
    clearInterval(this.interval);
  },
  render: function () {
    return (
      React.createElement('div', {className: 'container'},
        React.createElement('div', {id: this.id, ref: "randomQuote"},
          React.createElement('div', {className: 'row'},
            React.createElement('div', {className: 'col-xs-12 col-md-8 col-md-offset-2'},
              React.createElement('h3', null)
            )
          ),
          React.createElement('div', {className: 'row'},
            React.createElement('div', {className: 'col-xs-12 col-md-8 col-md-offset-2'},
              React.createElement('div', {className: 'spinner', style: {display: this.state.show ? 'none' : 'block'}},
                React.createElement('div', {className: 'double-bounce1'}),
                React.createElement('div', {className: 'double-bounce2'})
              ),
              React.createElement('blockquote', {
                  className: 'fadein',
                  ref: 'quote',
                  style: {display: this.state.show ? 'block' : 'none'}
                },
                React.createElement('p', null, this.state.quote),
                React.createElement('footer', null, this.state.author)
              )
            )
          )
        )
      )
    )
  }
});