var React = require('react/addons');
var TechnologyItem = require('./TechnologyItem.react');

module.exports = TechnologyList = React.createClass({
  displayName: 'TechnologyList',

  render: function () {
    var items = this.props.technologies;
    if (this.props.limit) {
      items = this.props.technologies.slice(0, this.props.limit);
    }

    var _ = React.DOM;
    var TechnologyList = [];
    if (!!this.props.technologies) {
      items.forEach(function (technology) {
        TechnologyList.push( React.createElement(TechnologyItem, {technology: technology}));
      }, this)
    }
    return (
      _.div({className: 'technology-list text-center'}, TechnologyList)
    );
  }
});