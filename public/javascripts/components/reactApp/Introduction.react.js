var React = require('react/addons');

module.exports = Introduction = React.createClass({
  displayName: 'Introduction',

  getInitialState: function () {
    return {
      technologiesModalToggle: this.props.technologiesModalToggle
    }
  },
  render: function () {
    return (
      React.createElement('div', {id: 'introduction'},
        React.createElement('div', {className: 'background-image'}),
        React.createElement('div', {className: 'col-xs-12 col-md-4 col-md-offset-1 profile'},
          React.createElement('div', {className: 'row text-center'},
            React.createElement('div', {className: 'col-xs-12'},
              React.createElement('img', {className: 'img thumbnail', src: '/images/me.jpg', height: '200px'})
            ),

            React.createElement('div', {className: 'col-xs-12 main'},
                React.createElement('div', {className: 'panel panel-default'},
                  React.createElement('div', {className: 'panel-body'},
                    React.createElement('div', {className: 'info'}, 'Paul Seleznev')
                  )
                )
            ),

            React.createElement('div', {className: 'col-xs-12 social'},
              React.createElement('div', {className: 'panel panel-default'},
                React.createElement('div', {className: 'panel-heading'},
                  React.createElement('h3', {className: 'panel-title'},
                    React.createElement('a', {className: 'header-link btn btn-link'}, 'Social links...')
                  )
                ),
                React.createElement('div', {className: 'panel-body'},
                  React.createElement('div', {className: 'btn-group btn-group-justified'},
                    React.createElement('a', {className: 'btn btn-primary'},
                      React.createElement('i', {className: 'fa fa-2x fa-facebook'})
                    ),
                    React.createElement('a', {className: 'btn btn-primary'},
                      React.createElement('i', {className: 'fa fa-2x fa-google-plus'})
                    ),
                    React.createElement('a', {className: 'btn btn-primary'},
                      React.createElement('i', {className: 'fa fa-2x fa-facebook'})
                    )
                  )
                )
              )
            ),

            React.createElement('div', {className: 'col-xs-12 technologies'},
              React.createElement('div', {className: 'panel panel-default'},
                React.createElement('div', {className: 'panel-heading'},
                  React.createElement('h3', {className: 'panel-title'},
                    React.createElement('a', {className: 'header-link btn btn-link', onClick: this.state.technologiesModalToggle}, 'Technologies...')
                  )
                ),
                React.createElement('div', {className: 'panel-body hidden-sm hidden-xs text-center'},
                  React.createElement('a', {href: '#', className: 'technology-link'}, 'JavaScript'),
                  React.createElement('a', {href: '#', className: 'technology-link'}, 'PHP'),
                  React.createElement('a', {href: '#', className: 'technology-link'}, 'HTML'),
                  React.createElement('a', {href: '#', className: 'technology-link'}, 'JavaScript'),
                  React.createElement('a', {href: '#', className: 'technology-link'}, 'PHP'),
                  React.createElement('a', {href: '#',  className: 'technology-link'}, 'HTML'),
                  React.createElement('a', {href: '#',  className: 'technology-link'}, 'JavaScript'),
                  React.createElement('a', {href: '#',  className: 'technology-link'}, 'PHP'),
                  React.createElement('a', {href: '#',  className: 'technology-link'}, 'HTML'),
                  React.createElement('a', {href: '#',  className: 'technology-link'}, 'JavaScript'),
                  React.createElement('a', {href: '#',  className: 'technology-link'}, 'PHP'),
                  React.createElement('a', {href: '#',  className: 'technology-link'}, 'HTML')
                )
              )
            )

            //React.createElement('div', {className: 'col-xs-12 quotes'},
            //  React.createElement('h6', null, 'Quote of the moment:'),
            //  React.createElement('blockquote', null,
            //    React.createElement('p', null, 'Some quote'),
            //    React.createElement('footer', null, 'Paul Seleznev')
            //  )
            //)
          )
        ),

        React.createElement('div', {className: 'col-md-4 col-md-offset-1 hidden-sm hidden-xs introduction-text-container'},
          React.createElement('div', {className: 'row text-center'},
            React.createElement('div', {className: 'col-md-12'},
              React.createElement('p', null,
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu finibus ligula. ' +
                'Maecenas interdum pulvinar tincidunt. Donec non feugiat enim, non vehicula risus. ' +
                'Nullam varius gravida nisi, scelerisque tincidunt mi pretium non. Integer ut justo ex. ' +
                'Fusce eget blandit libero. Pellentesque habitant morbi tristique senectus et netus et ' +
                'malesuada fames ac turpis egestas. Pellentesque sed nisl quis libero maximus cursus.' +
                ' Nullam consectetur nisi orci, in hendrerit quam finibus quis. ' +
                'Cras enim ante, sollicitudin in dictum et, ullamcorper nec augue. Duis sodales quam vel ante varius rhoncus.' +
                ' Phasellus nec tortor eu eros iaculis venenatis. Praesent quis metus sit amet eros molestie placerat. ' +
                'Vivamus molestie diam lacus, vel feugiat augue venenatis eget. Ut rhoncus convallis orci at interdum.' +
                ' Ut eu arcu a diam pharetra pharetra.'
              ),
              React.createElement('p', null,
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu finibus ligula. ' +
                'Maecenas interdum pulvinar tincidunt. Donec non feugiat enim, non vehicula risus. ' +
                'Nullam varius gravida nisi, scelerisque tincidunt mi pretium non. Integer ut justo ex. ' +
                'Fusce eget blandit libero. Pellentesque habitant morbi tristique senectus et netus et ' +
                'malesuada fames ac turpis egestas. Pellentesque sed nisl quis libero maximus cursus.' +
                ' Nullam consectetur nisi orci, in hendrerit quam finibus quis. ' +
                'Cras enim ante, sollicitudin in dictum et, ullamcorper nec augue. Duis sodales quam vel ante varius rhoncus.' +
                ' Phasellus nec tortor eu eros iaculis venenatis. Praesent quis metus sit amet eros molestie placerat. ' +
                'Vivamus molestie diam lacus, vel feugiat augue venenatis eget. Ut rhoncus convallis orci at interdum.' +
                ' Ut eu arcu a diam pharetra pharetra.'
              )
            ),
            React.createElement('div', {className: 'col-md-12'},
              React.createElement('a', {className: 'btn btn-default'}, 'Read all...')
            )
          )
        )
      )
    )
  }
});