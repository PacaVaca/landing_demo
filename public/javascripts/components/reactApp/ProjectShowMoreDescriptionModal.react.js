var React = require('react/addons');

module.exports = ProjectShowMoreDescriptionModal = React.createClass({
  displayName: 'ProjectShowMoreDescriptionModal',
  render: function () {
    var _ = React.DOM;
    return (
      _.div({className: 'modal fade', id: 'project-description-more-modal', role: 'dialog'},
        _.div({className: 'modal-dialog modal-lg'},
          _.div({className: 'modal-content'},
            _.div({className: 'modal-header'},
              _.button({type: 'button', className: 'close', 'data-dismiss': 'modal', 'area-label': 'Close'},
                _.span({'aria-hidden': true}, 'x')
              ),
              _.h4({className: 'modal-title'}, 'Full description')
            ),
            _.div({className: 'modal-body text-center'}, this.props.description)
          )
        )
      )
    )
  }
});