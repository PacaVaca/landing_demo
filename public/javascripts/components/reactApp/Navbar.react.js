var React = require('react/addons');

module.exports = Navbar = React.createClass({
  displayName: 'Navbar',

  technogolyClick: function (event) {
    event.preventDefault();
    this.props.technologiesModalToggle([]);
    $('.navbar-toggle').click();
  },

  render: function () {
    var _ = React.DOM;
    return (
      _.div(null,
        _.nav({className: 'navbar navbar-default navbar-fixed-top', role: 'navigation'},
          _.div({id: 'navbar'},
            _.div({className: 'navbar-header'},
              _.button({
                  className: 'navbar-toggle collapsed',
                  type: 'button',
                  'data-toggle': 'collapse',
                  'data-target': '#menu-navbar'
                },
                _.span({className: 'sr-only'}, 'Toggle navigation'),
                _.span({className: 'icon-bar'}),
                _.span({className: 'icon-bar'}),
                _.span({className: 'icon-bar'})
              ),
              _.a({className: 'navbar-brand', href: '#', alt: 'Brand'}, 'Brand')
            ),
            _.div({id: 'menu-navbar', className: 'collapse navbar-collapse'},
              _.ul({className: 'nav navbar-nav'},
                _.li({className: 'dropdown content-dropdown'},
                  _.a({
                      href: '#',
                      className: 'dropdown-toggle',
                      'data-toggle': 'dropdown',
                      role: 'button',
                      'aria-expanded': false
                    },
                    _.span(null, 'Content'),
                    _.span({className: 'caret'})
                  ),
                  React.createElement('ul', {className: 'dropdown-menu', role: 'menu'},
                    React.createElement('li', null,
                      React.createElement('a', {onClick: this.closeDropdowns, href: '/#react-app'}, 'Forewords')
                    ),
                    React.createElement('li', null,
                      React.createElement('a', {onClick: this.closeDropdowns, href: '/#portfolio'}, 'Portfolio')
                    ),
                    React.createElement('li', null,
                      React.createElement('a', {onClick: this.closeDropdowns, href: '/#contact-form'}, 'Contact Form')
                    )
                  )
                ),
                _.li({'data-toggle': 'modal'},
                  _.a({href: '#', onClick: this.technogolyClick}, 'Technologies...')
                ),
                React.createElement('li', {'data-toggle': 'modal', 'data-target': '#about-me-modal'},
                  React.createElement('a', {href: '#', onClick: this.closeDropdowns}, 'About Me')
                )
              )
            )
          )
        ),
        React.createElement('div', {
            id: 'about-me-modal',
            className: 'modal fade',
            role: 'dialog',
            'aria-labelledby': 'about-me-modal-title',
            'ariz-hidden': true
          },
          React.createElement('div', {className: 'modal-dialog modal-lg'},
            React.createElement('div', {className: 'modal-content'},
              React.createElement('div', {className: 'modal-header'},
                React.createElement('button', {className: 'close', type: 'button', 'data-dismiss': 'modal', 'aria-label': 'Close'},
                  React.createElement('span', {'aria-hidden': true}, 'x')
                )
              ),
              React.createElement('div', {className: 'modal-body'}),
              React.createElement('div', {className: 'modal-footer'})
            )
          )
        )
      )

    );
  }
});