var React = require('react/addons');

module.exports = TechnologyItem = React.createClass({
  displayName: 'TechnologyItem',

  render: function () {
    var _ = React.DOM;
    return (
      _.div({className: 'technology-item'},
        _.a({href: '#', className: 'technology-link'}, this.props.technology.title),
        _.div({className: 'technology-description'},
          _.p({className: 'experience'}, this.props.technology.experience)
        )
      )
    )
  }
});