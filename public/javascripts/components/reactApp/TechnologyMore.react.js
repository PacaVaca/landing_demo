var React = require('react/addons');

module.exports = TechnologyMore = React.createClass({
  displayName: 'TechnologyMore',

  showProjectTechnologies: function () {
    this.state.projectTechnologiesModal(this.state.technologies);
  },
  getInitialState: function () {
    return {
      technologies: this.props.technologies,
      projectTechnologiesModal: this.props.projectTechnologiesModal,
    };
  },
  render: function () {
    var _ = React.DOM;
    return (
      _.p({className: 'text-center col-xs-12 technology-more'},
        _.a({className: 'btn btn-link', onClick: this.showProjectTechnologies},
          _.i({className: 'fa fa-dot-circle-o'}),
          _.i({className: 'fa fa-dot-circle-o'}),
          _.i({className: 'fa fa-dot-circle-o'})
        )
      )
    )
  }
});