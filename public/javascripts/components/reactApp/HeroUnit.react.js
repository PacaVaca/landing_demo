var React = require('react/addons');

module.exports = HeroUnit = React.createClass({
  displayName: 'HeroUnit',
  getInitialState: function () {
    var skipHeight = 0;
    if (this.props.skipHeight) {
      skipHeight = this.props.skipHeight;
    }
    var height = $(window).height() || 500;
    return {
      heroUnitClass: this.props.heroUnitClass || 'hero-unit-default',
      height: height
    }
  },
  render: function () {
    return (
      React.createElement('div', {style: {height: this.state.height + 'px'}, className: 'container hero-unit ' + this.state.heroUnitClass, id: 'hero-unit'},
        React.createElement('div', {className: 'row'},
          React.createElement('div', {className: 'col-xs-12 col-md-8 col-md-offset-2'},
            React.createElement(SubscriptionForm, {
              dynamic: true,
              parentHeight: this.state.height,
              subscribe: this.props.subscribe,
              setEmail: this.props.setEmail,
              showTitle: true,
              email: this.props.email,
              btnClass: 'col-xs-12 col-md-8 col-md-offset-2'
            })
          )
        )
      )
    )
  }
});