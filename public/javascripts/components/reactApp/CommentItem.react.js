var React = require('react/addons');

module.exports = CommentItem = React.createClass({
  displayName: 'CommentItem',
  render: function () {
    return (
      React.createElement('div', null,
        React.createElement('blockquote', {className: 'col-xs-12 col-md-8 col-md-offset-2'},
          React.createElement('p', null, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum malesuada ' +
          'turpis nisl, eu placerat mauris iaculis nec. Etiam lacus leo, varius quis sagittis id, vulputate a ipsum. ' +
          'Vestibulum lobortis augue eget lectus sodales, at placerat dui molestie. Duis sit amet tincidunt ligula.' +
          ' Sed nec luctus mi. Sed vel convallis ex. Sed a urna auctor, placerat urna vitae, porta est.' +
          ' Sed tellus erat, commodo non aliquet sit amet, molestie a justo. Vivamus sed consequat enim. ' +
          'Integer eget tortor id leo tristique ultricies et blandit augue.'),
          React.createElement('footer', null, 'Some author')
        ),
        React.createElement('blockquote', {className: 'col-xs-12 col-md-8 col-md-offset-2'},
          React.createElement('p', null, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum malesuada ' +
          'turpis nisl, eu placerat mauris iaculis nec. Etiam lacus leo, varius quis sagittis id, vulputate a ipsum. ' +
          'Vestibulum lobortis augue eget lectus sodales, at placerat dui molestie. Duis sit amet tincidunt ligula.' +
          ' Sed nec luctus mi. Sed vel convallis ex. Sed a urna auctor, placerat urna vitae, porta est.' +
          ' Sed tellus erat, commodo non aliquet sit amet, molestie a justo. Vivamus sed consequat enim. ' +
          'Integer eget tortor id leo tristique ultricies et blandit augue.'),
          React.createElement('footer', null, 'Some author')
        ),
        React.createElement('blockquote', {className: 'col-xs-12 col-md-8 col-md-offset-2'},
          React.createElement('p', null, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum malesuada ' +
          'turpis nisl, eu placerat mauris iaculis nec. Etiam lacus leo, varius quis sagittis id, vulputate a ipsum. ' +
          'Vestibulum lobortis augue eget lectus sodales, at placerat dui molestie. Duis sit amet tincidunt ligula.' +
          ' Sed nec luctus mi. Sed vel convallis ex. Sed a urna auctor, placerat urna vitae, porta est.' +
          ' Sed tellus erat, commodo non aliquet sit amet, molestie a justo. Vivamus sed consequat enim. ' +
          'Integer eget tortor id leo tristique ultricies et blandit augue.'),
          React.createElement('footer', null, 'Some author')
        )
      )
    );
  }
});