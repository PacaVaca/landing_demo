var React = require('react/addons');
var TechnologyList = require('./TechnologyList.react');
var TechnologyMore = require('./TechnologyMore.react');

module.exports = ProjectItem = React.createClass({
  displayName: 'ProjectItem',
  showMoreDescription: function (event) {
    event.preventDefault();
    event.stopPropagation();
    this.props.showMoreDescription(this.state.project.description);
  },

  getInitialState: function () {
    return {
      projectTechnologiesModal: this.props.projectTechnologiesModal,
      project: this.props.project,
      technologies: this.props.project.technologies
    };
  },
  render: function () {
    var _ = React.DOM;
    return (
      _.div({className: 'col-xs-12'},
        _.div({className: 'project-item thumbnail'},
          _.img({className: 'image ', src: this.state.project.img}),
          _.div({className: 'row info'},
            _.div({className: 'form-group date-title col-xs-12'},
              _.span(null, 'Name:')
            ),
            _.div({className: 'form-group date-value text-right  col-xs-12'},
              _.span(null, this.state.project.title)
            ),
            _.div({className: 'form-group date-title col-xs-6'},
              _.span(null, 'startDate Date:')
            ),
            _.div({className: 'form-group date-title col-xs-6'},
              _.span(null, 'End Date:')
            ),
            _.div({className: 'form-group date-value text-right col-xs-6'},
              _.span(null, this.state.project.startDate)
            ),
            _.div({className: 'form-group  date-value text-right col-xs-6'},
              _.span(null, this.state.project.endDate)
            ),

            _.div({className: 'form-group col-xs-12'},
              _.div({className: 'description-container bottom-space-20'},
                _.div({className: 'description'}, this.state.project.description),
                _.div({className: 'description-more text-center'},
                  _.a({className: 'show-more btn btn-info', onClick: this.showMoreDescription}, 'Show more')
                )
              )
            )
            ,
            React.createElement(TechnologyList, {limit: 5, technologies: this.state.project.technologies}),
            React.createElement(TechnologyMore, {
              technologies: this.state.project.technologies,
              projectTechnologiesModal: this.state.projectTechnologiesModal
            })
          )
        )
      )
    );
  }
});