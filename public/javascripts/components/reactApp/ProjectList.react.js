var React = require('react/addons');
var ProjectItem = require('./ProjectItem.react');
var ProjectShowMoreDescriptionModal = require('./ProjectShowMoreDescriptionModal.react.js');

module.exports = ProjectList = React.createClass({
  displayName: 'ProjectList',

  showMoreProjectDescription: function (description) {
    this.setState({
      fullDescription: description
    });
    $('#project-description-more-modal').modal('toggle');
  },
  getInitialState: function () {
    return {
      projects: this.props.projects,
      technologiesModalToggle: this.props.technologiesModalToggle
    };
  },
  render: function () {
    var _ = React.DOM;
    var projectList = [];
    this.state.projects.forEach(function (project) {
      projectList.push(_.div({className: 'col-xs-12 col-md-4 col-sm-6'},
        _.div({className: 'row'},
          React.createElement(ProjectItem, {
            project: project,
            projectTechnologiesModal: this.state.technologiesModalToggle,
            showMoreDescription: this.showMoreProjectDescription
          })
        )
      ));
    }, this);
    return (
      _.div(null,
        React.createElement(ProjectShowMoreDescriptionModal, {description: this.state.fullDescription}),
        projectList
      )
    );
  }
});