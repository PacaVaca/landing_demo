var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var ContactEmailScheam = new Schema({
  firstname: String,
  lastname: String,
  email: String,
  topic: String,
  description: String
});

module.exports = ContactEmail = mongoose.model('ContactEmail', ContactEmailScheam);